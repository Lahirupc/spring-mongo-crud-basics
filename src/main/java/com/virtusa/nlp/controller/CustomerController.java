package com.virtusa.nlp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.nlp.model.Customer;
import com.virtusa.nlp.repository.CustomerRespository;

@RestController
public class CustomerController {

	@Autowired
	CustomerRespository customerRepository;
	
	@GetMapping("/customers")
	public List<Customer> getAllCustomers(){
		return customerRepository.findAll();
	}
	
	@GetMapping("/customers/{id}")
	public Optional<Customer> getCustomerById(@PathVariable String id) {
		return customerRepository.findById(id);
	}
	
	
	@PostMapping("/customers")
	public String createCustomer(@RequestBody Customer customer) {
		customerRepository.save(customer);
		return "Customer created successfully!";
	}
	
	
	@PutMapping("/customers/{id}")
	public String updateCustomer(@PathVariable String id, @RequestBody Customer customer) {
		Optional<Customer> customerDoc = customerRepository.findById(id);
		
		if(customerDoc.isPresent()) {
			Customer newCustomerObj = customerDoc.get();
			newCustomerObj.setFirstName(customer.getFirstName());
			newCustomerObj.setLastName(customer.getLastName());
			newCustomerObj.setEmail(customer.getEmail());
			customerRepository.save(newCustomerObj);
			
			return "Customer updated successfully!";
		} else {
			return "Update failed. Customer does not exist!";
		}
	}
	
	//################# Simple example #######################
	//	@DeleteMapping("/customers/{id}")
	//	public String deleteCustomer(@PathVariable String id) {
	//		customerRepository.deleteById(id);
	//		return "Customer deleted successfully!";
	//	}
	//####################################################################################
		
	/*################## Better example ############################*/
		@DeleteMapping("/customers/{id}")
		public ResponseEntity<String> deleteCustomer(@PathVariable("id") String id) {
			try {
			  customerRepository.deleteById(id);
			  return new ResponseEntity<String>("Customer deleted successfully!", HttpStatus.OK);
			  } catch (Exception e) {
				  return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		  }
		}
	 /*########################################################################################*/
	

	
	// Using the custom method we implemented
	@GetMapping("/customers/search")
	public List<Customer> getCustomerByName(@RequestBody Customer customer) {
		return customerRepository.findCustomerByLastName(customer.getLastName());
	}
	
}
