package com.virtusa.nlp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.virtusa.nlp.model.Customer;

public interface CustomerRespository extends MongoRepository<Customer, String>{
//	save(), findAll(), deleteById(), findById(), 
	@Query("{lastName:'?0'}")
	List<Customer> findCustomerByLastName(String lastName);
}
