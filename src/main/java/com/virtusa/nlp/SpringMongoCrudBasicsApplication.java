package com.virtusa.nlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMongoCrudBasicsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMongoCrudBasicsApplication.class, args);
	}

}
